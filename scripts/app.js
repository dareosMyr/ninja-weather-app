const cityForm = document.querySelector("form");
const card = document.querySelector(".card");
const details = document.querySelector(".details");
const timeImg = document.querySelector("img.time");
const icon = document.querySelector(".icon img");
const forcast = new Forcast();

const updateUI = (data) => {
  const { cityDets, conditions } = data;

  //update details template
  details.innerHTML = `
    <h5 class="my-3">${cityDets.EnglishName}</h5>
    <div class="my-3">${conditions.WeatherText}</div>
    <div class="display-4 my-4">
      <span>${conditions.Temperature.Imperial.Value}</span>
      <span>&deg;F</span>
    </div>
  `;

  //show card
  if (card.classList.contains("d-none")) {
    card.classList.remove("d-none");
  }

  //update images
  const iconSrc = `img/icons/${conditions.WeatherIcon}.svg`;

  let timeSrc = conditions.IsDayTime ? "img/day.svg" : "img/night.svg";

  timeImg.setAttribute("src", timeSrc);
  icon.setAttribute("src", iconSrc);
};

cityForm.addEventListener("submit", (e) => {
  //get city value
  e.preventDefault();
  const city = cityForm.city.value.trim().toLowerCase();
  cityForm.reset();

  forcast
    .updateCity(city)
    .then((data) => updateUI(data))
    .catch((err) => console.log("something went wrong:", err));

  //set local storage
  localStorage.setItem("city", city);
});

if (localStorage.city) {
  forcast
    .updateCity(localStorage.city)
    .then((data) => updateUI(data))
    .catch((err) => console.log("something went wrong:", err));
}
