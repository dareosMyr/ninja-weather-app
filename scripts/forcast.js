class Forcast {
  constructor() {
    this.key = "K1YEULixzhVbtNEpuUBdM8jmS3m2VTol";
    this.weatherURI = "http://dataservice.accuweather.com/currentconditions/v1/";
    this.cityURI = "http://dataservice.accuweather.com/locations/v1/cities/search";
  }

  async updateCity(city) {
    const cityDets = await this.getCity(city);
    const conditions = await this.getConditions(cityDets.Key);

    return {
      cityDets,
      conditions,
    };
  }

  async getCity(city) {
    const query = `?apikey=${this.key}&q=${city}`;
    const response = await fetch(this.cityURI + query);
    const data = await response.json();

    return data[0];
  }

  async getConditions(cityID) {
    const query = `${cityID}?apikey=${this.key}`;
    const response = await fetch(this.weatherURI + query);
    const data = await response.json();
    return data[0];
  }
}
